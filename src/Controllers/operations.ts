import {
  getProduct,
  getAllOperations,
  createOperation,
} from "../Repositories/database";
import { Operation } from "../types/operation";
import { operations } from "../enums/operations";

export async function getAlloperationsController() {
  try {
    const products = await getAllOperations();
    return products;
  } catch (error) {
    let message;
    if (error instanceof Error) {
      message = error.message;
    } else {
      message = String(error);
    }
    return { error: message };
  }
}

export async function sellProductController(productId: number, body: Operation) {
  try {
    const product = await getProduct(productId);
    const operation = createOperation(
      product.id,
      new Date(),
      body.numberOfProducts,
      product.sellPrice,
      body.numberOfProducts * product.sellPrice,
      operations.sellOperation,
    );
    return operation;
  } catch (error) {
    let message;
    if (error instanceof Error) {
      message = error.message;
    } else {
      message = String(error);
    }
    return { error: message };
  }
}

export async function buyProductController(productId: number, body: Operation) {
  try {
    const product = await getProduct(productId);
    const operation = createOperation(
      product.id,
      new Date(),
      body.numberOfProducts,
      product.buyPrice,
      body.numberOfProducts * product.buyPrice,
      operations.buyOperation,
    );
    return operation;
  } catch (error) {
    let message;
    if (error instanceof Error) {
      message = error.message;
    } else {
      message = String(error);
    }
    return { error: message };
  }
}
