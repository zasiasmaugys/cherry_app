import {
  getAllProducts,
  getProduct,
  createProduct,
  updateProduct,
} from "../Repositories/database";
import { Product } from "../types/product";

export async function getAllProductsController() {
  try {
    const products = await getAllProducts();
    return products;
    
  } catch (error) {
    let message;
    if (error instanceof Error) {
      message = error.message;
    } else {
      message = String(error);
    }
    return { error: message };
  }
}

export async function createProductController(body: Product) {
  try {
    const product = createProduct(body);
    return product;
  } catch (error) {
    let message;
    if (error instanceof Error) {
      message = error.message;
    } else {
      message = String(error);
    }
    return { error: message };
  }
}

export async function getProductController(productId: number) {
  try {
    return getProduct(productId);
  } catch (error) {
    return { error: `${error}` };
  }
}

export async function updateProductController(productId: number, body: Product) {
  try {
    const product = updateProduct(
      productId,
      body,
    );
    return product;
  } catch (error) {
    let message;
    if (error instanceof Error) {
      message = error.message;
    } else {
      message = String(error);
    }
    return { error: message };
  }
}
