import { getUser, createUser } from "../Repositories/database";
import { hashPassword, validateUser } from "../Services/hashing";
import jwt from "jsonwebtoken";
import { User } from "../types/user";
import dotenv from "dotenv";
dotenv.config();

export async function registerUserController(body: User) {  
  const hashedPassword = await hashPassword(body.userPassword);
  const user = await createUser(
    body.userName,
    hashedPassword as string,
    body.permissions,
  );
  return user;
}

export async function loginUserController(body: User) {
  try {
    const user = await getUser(body.userName);
    if (user === undefined) {
      throw Error("User not found");
    }
    const validated = await validateUser(body.userPassword, user.userPassword);
    if (validated === true) {
      const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET as string);
      return { accessToken: accessToken };
    } else {
      throw Error("Wrong password");
    }
  } catch (error) {
    let message;
    if (error instanceof Error) {
      message = error.message;
    } else {
      message = String(error);
    }
    return { error: message };
  }
}
