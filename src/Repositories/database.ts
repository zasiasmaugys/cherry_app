import dotenv from "dotenv";
dotenv.config();
import { Product, ReturnedProduct } from "../types/product";
import { OperationType } from "../types/operation";
// import { User } from "../types/user";
import { Logs } from "../types/logs";
import { Sequelize } from "sequelize";
import { userModel } from "../models/user.model";
import { productModel } from "../models/product.model";
import { operationModel } from "../models/operation.model";
import { logModel } from "../models/log.model";

export const sequelize = new Sequelize(
  process.env.MYSQL_DATABASE!,
  process.env.MYSQL_USER!,
  process.env.MYSQL_PASSWORD,
  {
    host: process.env.MYSQL_HOST,
    dialect: "mysql",
  },
);

sequelize.authenticate().then(() => {
  console.log("Connection has been established successfully.");
}).catch(error => {
  console.error("Unable to connect to the database: ", error);
});

export async function getAllProducts() {
  const products = await productModel.findAll();
  
  return products;
}

export async function getProduct(id: number) {
  const returnedProduct = await productModel.findByPk(id);
  if (returnedProduct === null) {
    throw Error("Product not found!");
  }
  
  return returnedProduct.dataValues as ReturnedProduct;
}

export async function createProduct(productBody: Product) {
  const createdProduct = await productModel.create({ title: productBody.title, description: productBody.description, buyPrice: productBody.buyPrice, sellPrice: productBody.sellPrice });

  return createdProduct;
}

export async function updateProduct(
  productId: number,
  productBody: Product,
) {
  const updatedProduct = await productModel.update({ title: productBody.title, description: productBody.description, buyPrice: productBody.buyPrice, sellPrice: productBody.sellPrice }, {
    where: {
      id: productId,
    },
  });
  if (updatedProduct[0] === 0) {
    throw Error("Product was not found!");
  }

  return "Product was updated";
}

export async function getAllOperations() {
  const returnedOperations = await operationModel.findAll();

  return returnedOperations;
}

export async function createOperation(
  productId: number,
  date: Date,
  numberOfProducts: number,
  price: number,
  priceOfAllProducts: number,
  operation: OperationType["operation"],
) {
  const createdOperation = await operationModel.create({ productId, operationDate: date, numberOfProducts, oneProductPrice: price, priceOfAllProducts, operation });

  return createdOperation;
}

export async function getUser(userName: string) {
  const returnedUser = await userModel.findOne({ where: { userName } });
  if (returnedUser === null) {
    throw Error("Not found!");
  }

  return returnedUser.dataValues;
}

export async function createUser(userName: string, userPassword: string, permissions: string) {
  const savedUserResponse = await userModel.create({ userName, userPassword, permissions });

  return savedUserResponse instanceof userModel;
}

export async function createLog(log: Logs) {
  const createdLog = await logModel.create({ method: log.method, url: log.url, requestDate: log.requestDate, logInformation: log.logInformation });
  
  return createdLog;
}
