import express from "express";
import {
  getAlloperationsController,
  sellProductController,
  buyProductController,
} from "../Controllers/operations";
import { authenticateToken } from "../Services/token";
import { authorizeUser } from "../Services/userAuth";
import { logger } from "../Services/logger";
import { roles } from "../enums/roles";
import { sellAndBuyValidator } from "../Services/validators";

const router = express.Router();

router.get(
  "/get-operations",
  logger("Get all opertaions request"),
  authenticateToken,
  (_req, res) => {
    getAlloperationsController()
      .then(result => res.send(result))
      .catch(err => res.status(500).send(err));
  },
);

router.get(
  "/sell/:id",
  logger("Sell product request"),
  authenticateToken,
  authorizeUser(roles.admin),
  sellAndBuyValidator,
  (req, res) => {
    sellProductController(Number(req.params.id), req.body)
      .then(result => res.send(result))
      .catch(err => res.status(500).send(err));
  },
);

router.get(
  "/buy/:id",
  logger("Buy product request"),
  authenticateToken,
  authorizeUser(roles.admin),
  sellAndBuyValidator,
  (req, res) => {
    buyProductController(Number(req.params.id), req.body)
      .then(result => res.send(result))
      .catch(err => res.status(500).send(err));
  },
);

export default router;
