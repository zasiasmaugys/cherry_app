import express from "express";
import {
  getAllProductsController,
  getProductController,
  createProductController,
  updateProductController,
} from "../Controllers/products";
import { authenticateToken } from "../Services/token";

const router = express.Router();

router.get("/get-products", authenticateToken, (req, res) => {
  getAllProductsController()
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});

router.get("/:id", authenticateToken, (req, res) => {
  getProductController(Number(req.params.id))
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});

router.put("/create", authenticateToken, (req, res) => {
  createProductController(req.body)
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});

router.put("/update/:id", authenticateToken, (req, res) => {
  updateProductController(Number(req.params.id), req.body)
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});

// router.delete("/delete/:id", (req, res) => {
//   res.send("delete product");
// });

export default router;
