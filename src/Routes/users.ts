import express, { Request, Response } from "express";
import {
  registerUserController,
  loginUserController,
} from "../Controllers/users";
import { userRegisterValidation, userRegisterValidation2 } from "../Services/validators";

const router = express.Router();

router.get("/register", userRegisterValidation, async (req: Request, res: Response) => {
  try {
    const user = userRegisterValidation2(req.body);
    const result = await registerUserController(user);
    res.send(result);
  } catch (err) {
    res.status(500).send(`${err}`);
  }
});

router.post("/login", (req, res: Response<{ accesstoken?: string; error?: string }>) => {
  loginUserController(req.body)
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});

export default router;
