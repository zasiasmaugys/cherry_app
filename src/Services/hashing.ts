import bcrypt from "bcrypt";
const saltRounds = 10;

export function hashPassword(password: string) {
  return bcrypt
    .hash(password, saltRounds)
    .catch(err => console.error(err.message));
}

export function validateUser(password: string, hash: string) {
  return bcrypt
    .compare(password, hash)
    .catch(err => console.error(err.message));
}
