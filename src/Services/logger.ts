import { createLog } from "../Repositories/database";
import { Request, Response, NextFunction } from "express";

export function logger(logInformation: string) {
  return (req: Request, res: Response, next: NextFunction) => {
    const fullUrl = req.protocol + "://" + req.get("host") + req.originalUrl;
    createLog({ method: req.method, url: fullUrl, requestDate: new Date(), logInformation });
    next();
  };
}
