import { Request, Response, NextFunction } from "express";

export function authorizeUser(role: string) {
  return (req: Request, res: Response, next: NextFunction) => {
    if (res.locals.user.permissions !== role) {
      res.status(401);
      return res.send("Not allowed");
    }
    next();
  };
}
