import Joi from "joi";
import { Request, Response, NextFunction } from "express";
import { User } from "../types/user";
import { sellAndBuyBody } from "../types/operation";

const userRegisterSchema = Joi.object<User>({
  userName: Joi.string().required().min(3),
  userPassword: Joi.string().required().min(6),
  permissions: Joi.string().required().valid("user", "admin"),
});

const sellAndBuySchema = Joi.object<sellAndBuyBody>({
  numberOfProducts: Joi.number().required().min(1).positive(),
});

export function userRegisterValidation(req: Request, res: Response, next: NextFunction) {
  const { error, value } = userRegisterSchema.validate(req.body);
  if (error) {
    res.json(error.details[0].message);
  } else {
    console.log(value);
    next();
  }
}

export function userRegisterValidation2(body: unknown) {
  const { error, value } = userRegisterSchema.validate(body);
  if (error) {
    throw error;
  }
  return value;
}

export function sellAndBuyValidator(req: Request, res: Response, next: NextFunction) {
  const { error, value } = sellAndBuySchema.validate(req.body);
  if (error) {
    res.json(error.details[0].message);
  } else {
    console.log(value);
    next();
  }
}
