import { Sequelize, DataTypes, Model, InferAttributes, InferCreationAttributes } from "sequelize";
import dotenv from "dotenv";
dotenv.config();

interface LogModel extends Model<InferAttributes<LogModel>, InferCreationAttributes<LogModel>> {
  method: string;
  url: string;
  requestDate: Date;
  logInformation: string;
}

export const sequelize = new Sequelize(
  process.env.MYSQL_DATABASE!,
  process.env.MYSQL_USER!,
  process.env.MYSQL_PASSWORD,
  {
    host: process.env.MYSQL_HOST,
    dialect: "mysql",
  },
);

sequelize.authenticate().then(() => {
  console.log("Connection has been established successfully.");
}).catch(error => {
  console.error("Unable to connect to the database: ", error);
});

export const logModel = sequelize.define<LogModel>("logs", {
  // Model attributes are defined here
  method: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  url: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  requestDate: {
    type: DataTypes.DATE,
    allowNull: false,
  },
  logInformation: {
    type: DataTypes.STRING,
    allowNull: false,
    // allowNull defaults to true
  },
}, {
  timestamps: false,
  // Other model options go here
});
