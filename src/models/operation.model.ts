import { Sequelize, DataTypes, Model, InferAttributes, InferCreationAttributes } from "sequelize";
import dotenv from "dotenv";
dotenv.config();

interface OperationModel extends Model<InferAttributes<OperationModel>, InferCreationAttributes<OperationModel>> {
  productId: number;
  operationDate: Date;
  numberOfProducts: number;
  oneProductPrice: number;
  priceOfAllProducts: number;
  operation: string;
}

export const sequelize = new Sequelize(
  process.env.MYSQL_DATABASE!,
  process.env.MYSQL_USER!,
  process.env.MYSQL_PASSWORD,
  {
    host: process.env.MYSQL_HOST,
    dialect: "mysql",
  },
);

sequelize.authenticate().then(() => {
  console.log("Connection has been established successfully.");
}).catch(error => {
  console.error("Unable to connect to the database: ", error);
});

export const operationModel = sequelize.define<OperationModel>("operation", {
  // Model attributes are defined here
  productId: {
    type: DataTypes.NUMBER,
    allowNull: false,
  },
  operationDate: {
    type: DataTypes.DATE,
    allowNull: false,
  },
  numberOfProducts: {
    type: DataTypes.NUMBER,
    allowNull: false,
  },
  oneProductPrice: {
    type: DataTypes.NUMBER,
    allowNull: false,
  },
  priceOfAllProducts: {
    type: DataTypes.NUMBER,
    allowNull: false,
  },
  operation: {
    type: DataTypes.STRING,
    allowNull: false,
    // allowNull defaults to true
  },
}, {
  timestamps: false,
  // Other model options go here
});
