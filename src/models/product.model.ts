import { Sequelize, DataTypes, 
  // Model, InferAttributes, InferCreationAttributes 
} from "sequelize";
import dotenv from "dotenv";
dotenv.config();

// interface ProductModel extends Model<InferAttributes<ProductModel>, InferCreationAttributes<ProductModel>> {
//   title: string;
//   description: string;
//   buyPrice: number;
//   sellPrice: number;
// }

export const sequelize = new Sequelize(
  process.env.MYSQL_DATABASE!,
  process.env.MYSQL_USER!,
  process.env.MYSQL_PASSWORD,
  {
    host: process.env.MYSQL_HOST,
    dialect: "mysql",
  },
);

sequelize.authenticate().then(() => {
  console.log("Connection has been established successfully.");
}).catch(error => {
  console.error("Unable to connect to the database: ", error);
});

export const productModel = sequelize.define("product", {
  // Model attributes are defined here
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  buyPrice: {
    type: DataTypes.NUMBER,
    allowNull: false,
  },
  sellPrice: {
    type: DataTypes.NUMBER,
    allowNull: false,
    // allowNull defaults to true
  },
}, {
  timestamps: false,
  // Other model options go here
});
