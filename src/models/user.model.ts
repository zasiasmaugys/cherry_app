import { Sequelize, DataTypes, Model, InferAttributes, InferCreationAttributes } from "sequelize";
import dotenv from "dotenv";
dotenv.config();

interface UserModel extends Model<InferAttributes<UserModel>, InferCreationAttributes<UserModel>> {
  userName: string;
  userPassword: string;
  permissions: string;
}

// Define user in function. Import sequelizer and use it to define users Model.
export const sequelize = new Sequelize(
  process.env.MYSQL_DATABASE!,
  process.env.MYSQL_USER!,
  process.env.MYSQL_PASSWORD,
  {
    host: process.env.MYSQL_HOST,
    dialect: "mysql",
  },
);

sequelize.authenticate().then(() => {
  console.log("Connection has been established successfully.");
}).catch(error => {
  console.error("Unable to connect to the database: ", error);
});

export const userModel = sequelize.define<UserModel>("users", {
  // Model attributes are defined here
  userName: {
    type: DataTypes.STRING(50),
    allowNull: false,
  },
  userPassword: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  permissions: {
    type: DataTypes.STRING,
    allowNull: false,
    // allowNull defaults to true
  },
}, {
  timestamps: false,
  // Other model options go here
});
