import express from "express";

import productsRouter from "./Routes/products";
import operationsRouter from "./Routes/operations";
import usersRouter from "./Routes/users";
import morgan from "morgan";

const port = process.env.PORT || 3000;
const app = express();

app.use(express.json()); // <==== parse request body as JSON
app.use(
  morgan(":method :url :status :res[content-length] - :response-time ms"),
);

app.use((req, res, next) => {
  console.log("Time: ", new Date().toISOString());
  next();
});

app.use("/users", usersRouter);
app.use("/products", productsRouter);
app.use("/operations", operationsRouter);

app.get("*", (req, res) => {
  res.send("not implemented");
});

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

