export interface Logs {
    method: string;
    url: string;
    requestDate: Date;
    logInformation: string;
  }
