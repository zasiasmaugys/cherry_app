export interface Operation {
  productId: number;
  date: string;
  numberOfProducts: number;
  price: number;
  priceOfAllProducts: number;
}

export interface OperationType {
  operation: "buy" | "sell";
}

export interface sellAndBuyBody {
  numberOfProducts: number;
}
