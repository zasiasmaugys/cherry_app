export interface Product {
  title: string;
  description: string;
  buyPrice: number;
  sellPrice: number;
}

export interface ReturnedProduct extends Product {
  id: number;
  title: string;
  description: string;
  buyPrice: number;
  sellPrice: number;
}
