export interface User {
  userName: string;
  userPassword: string;
  permissions: "admin" | "user";
}

export interface UserRes {
  id: number;
  userName: string;
  permissions: "admin" | "user";
}
